package com.atlassian.fastdev.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

/**
 * @since version
 */
public class StreamEater extends Thread
{
    private final InputStream is;
    private final ConcurrentLinkedQueue<String> output;
    private boolean killIt;

    public StreamEater(InputStream is, ConcurrentLinkedQueue<String> output)
    {
        this.is = is;
        this.output = output;
        this.killIt = false;
        start();
    }

    public void run()
    {
        BufferedReader br = null;
        StringBuffer lineBuffer = null;
        try
        {
            br = new BufferedReader(new InputStreamReader(is));
            int c;
            lineBuffer = new StringBuffer();

            while ((c = br.read()) > -1 && !killIt)
            {
                lineBuffer.append((char) c);

                if ((c == '\n') || (c == '\r'))
                {
                    String line = lineBuffer.toString();
                    output.add(line);
                    lineBuffer = new StringBuffer();
                }
            }
        }
        catch (IOException ioe)
        {
            //eat some more.
        }
        finally {

            if(null != lineBuffer)
            {
                output.add(lineBuffer.toString());    
            }
            
            if(null != br)
            {
                IOUtils.closeQuietly(br);
            }
        }
    }
    
    public void kill()
    {
        this.killIt = true;
    }
}