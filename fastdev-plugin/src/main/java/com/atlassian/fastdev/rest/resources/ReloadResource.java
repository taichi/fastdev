package com.atlassian.fastdev.rest.resources;

import java.io.IOException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fastdev.ReloadHandler;
import com.atlassian.fastdev.maven.MavenTaskManager;
import com.atlassian.fastdev.rest.FastdevUriBuilder;
import com.atlassian.fastdev.rest.representations.MavenTaskCollectionRepresentation;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/reload")
@AnonymousAllowed
public class ReloadResource
{
    private final MavenTaskManager taskManager;
    private final FastdevUriBuilder uriBuilder;
    private final ReloadHandler reloadHandler;

    public ReloadResource(MavenTaskManager taskManager, FastdevUriBuilder uriBuilder, ReloadHandler reloadHandler)
    {
        this.taskManager = checkNotNull(taskManager, "taskManager");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
        this.reloadHandler = checkNotNull(reloadHandler, "reloadHandler");
    }

    @POST
    @Produces(APPLICATION_JSON)
    public Response triggerReloadTask() throws IOException
    {
        for (String output : reloadHandler.reloadPlugins())
        {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            String json = gson.toJson(new MavenTaskCollectionRepresentation(taskManager, uriBuilder));
            
            return Response.ok(json, MediaType.APPLICATION_JSON).build();
        }

        return Response.noContent().build();
    }

    @Path("/withtests")
    @POST
    @Produces(APPLICATION_JSON)
    public Response triggerReloadWithTestsTask() throws IOException
    {
        for (String output : reloadHandler.reloadPluginsWithTests())
        {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            String json = gson.toJson(new MavenTaskCollectionRepresentation(taskManager, uriBuilder));

            return Response.ok(json, MediaType.APPLICATION_JSON).build();
            
        }

        return Response.noContent().build();
    }
}

