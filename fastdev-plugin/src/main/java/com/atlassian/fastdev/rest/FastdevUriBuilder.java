package com.atlassian.fastdev.rest;

import com.atlassian.fastdev.maven.MavenTask;
import com.atlassian.fastdev.rest.resources.MavenTaskResource;
import com.atlassian.sal.api.ApplicationProperties;
import com.sun.jersey.api.uri.UriBuilderImpl;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class FastdevUriBuilder
{
    private final ApplicationProperties applicationProperties;

    public FastdevUriBuilder(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public final URI buildMavenTaskCollectionUri()
    {
        return newBaseUriBuilder().path(MavenTaskResource.class).build();
    }

    public final URI buildMavenTaskUri(MavenTask mavenTask)
    {
        return newBaseUriBuilder().path(MavenTaskResource.class).path(mavenTask.getUuid().toString()).build();
    }

    // Note that this will NOT add the context path (i.e. "upm" in our case), given a relative URI.
    public final URI makeAbsolute(URI uri)
    {
        if (uri.isAbsolute())
        {
            return uri;
        }
        return URI.create(applicationProperties.getBaseUrl()).resolve(uri).normalize();
    }

    protected UriBuilder newBaseUriBuilder()
    {
        return newApplicationBaseUriBuilder().path("/rest/fastdev/1.0");
    }

    protected UriBuilder newApplicationBaseUriBuilder()
    {
        URI base = URI.create(applicationProperties.getBaseUrl()).normalize();
        return new UriBuilderImpl().path(base.getPath());
    }



}
