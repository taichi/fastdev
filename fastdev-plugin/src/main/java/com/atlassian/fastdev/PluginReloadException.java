package com.atlassian.fastdev;

import java.io.File;

/**
 * If any problem happens during a plugin reload
 */
public class PluginReloadException extends Exception
{
    private final File root;

    public PluginReloadException(File root, String message)
    {
        super(message);
        this.root = root;
    }

    public File getRoot()
    {
        return root;
    }
}
