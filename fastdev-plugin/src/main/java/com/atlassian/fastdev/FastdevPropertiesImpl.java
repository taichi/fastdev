package com.atlassian.fastdev;

import org.osgi.framework.BundleContext;

import static com.atlassian.fastdev.util.Option.option;

public class FastdevPropertiesImpl implements FastdevProperties
{
    private final String fastdevVersion;

    public FastdevPropertiesImpl(final BundleContext bundleContext)
    {
        this.fastdevVersion = option(bundleContext.getBundle().getHeaders().get("Bundle-Version")).getOrElse("Unknown").toString();
    }

    @Override
    public String getFastdevVersion()
    {
        return fastdevVersion;
    }
}
