package com.atlassian.fastdev;

public interface FastdevProperties
{
    String FASTDEV_MVN_COMMAND = "fastdev.mvn.command";
    String EXTRA_NO_RELOAD_EXTENSIONS_PROP = "fastdev.no.reload.extensions";
    String EXTRA_NO_RELOAD_FILES_PROP = "fastdev.no.reload.files";
    String EXTRA_NO_RELOAD_DIRECTORIES_PROP = "fastdev.no.reload.directories";
    String PLUGIN_ROOT_DIRECTORIES_PROP = "plugin.root.directories";
    String FASTDEV_USERNAME_PROP = "fastdev.install.username";
    String FASTDEV_PASSWORD_PROP = "fastdev.install.password";

    String getFastdevVersion();
}
