package com.atlassian.fastdev.maven;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

import static org.apache.commons.lang.SystemUtils.IS_OS_WINDOWS;

/**
 * @since version
 */
public class DefaultMavenTask implements MavenTask
{
    private final MavenTaskManager mavenTaskManager;
    private final ExecutorService executorService;
    private final File buildRoot;
    private final ProcessBuilder builder;
    private final UUID uuid;
    private final ConcurrentLinkedQueue<String> output;
    private volatile Integer exitCode;
    private final long currentAverage;
    private volatile long startTime;

    DefaultMavenTask(UUID uuid, File buildRoot, List<String> commands,MavenTaskManager mavenTaskManager, ExecutorService executorService)
    {
        this.mavenTaskManager = mavenTaskManager;
        this.executorService = executorService;
        
        ImmutableList.Builder<String> commandsBuilder = ImmutableList.builder();
        if (IS_OS_WINDOWS)
        {
            // FASTDEV-77: Windows tries to run the "mvn" (Unix) sh script
            commandsBuilder.add("cmd", "/c");
        }
        commandsBuilder.add(mavenTaskManager.getMavenCommand());
        commandsBuilder.addAll(commands);

        this.builder = new ProcessBuilder(commandsBuilder.build());

        // execute the process in the specified buildRoot
        builder.directory(buildRoot);

        // hack to turn off Don's colorizer for those that have it installed
        builder.environment().remove("MAVEN_COLOR");
        this.uuid = uuid;
        this.output = new ConcurrentLinkedQueue<String>();
        this.buildRoot = buildRoot;
        this.currentAverage = mavenTaskManager.averageBuildTime(buildRoot);
    }

    @Override
    public File getBuildRoot()
    {
        return buildRoot;
    }

    @Override
    public long getAverageTaskTime()
    {
        return currentAverage;
    }

    @Override
    public UUID getUuid()
    {
        return uuid;
    }

    @Override
    public Iterable<String> getOutput()
    {
        return ImmutableList.copyOf(output);
    }

    @Override
    public Integer getExitCode()
    {
        return exitCode;
    }

    @Override
    public void setExitCode(Integer code)
    {
        this.exitCode = code;
    }

    @Override
    public long getElapsedTime()
    {
        return System.currentTimeMillis() - startTime;
    }

    @Override
    public long getStartTime()
    {
        return startTime;
    }

    @Override
    public Future<Integer> start()
    {
        return executorService.submit(new Callable<Integer>()
        {
            public Integer call() throws Exception
            {
                try
                {
                    Process process = builder.start();
                    mavenTaskManager.LOG.info("Maven process executed as : " + Joiner.on(" ").join(builder.command()));
                    startTime = System.currentTimeMillis();
                    InputStreamReader isr = new InputStreamReader(process.getInputStream());
                    BufferedReader br = new BufferedReader(isr);

                    // The output needs to be consumed, else the process will hang
                    String lineRead;
                    while ((lineRead = br.readLine()) != null)
                    {
                        output.add(lineRead);
                    }

                    // Block until the process is finished
                    process.waitFor();

                    exitCode = process.exitValue();
                    // Then signal that the task is done by removing the mapping from the manager
                    mavenTaskManager.remove(DefaultMavenTask.this);
                    mavenTaskManager.recordBuildTime(buildRoot, System.currentTimeMillis() - startTime);
                    return process.exitValue();
                }
                catch(Exception e)
                {
                    mavenTaskManager.LOG.error("Maven execution failed", e);
                    throw e;
                }
            }
        });
    }
}
