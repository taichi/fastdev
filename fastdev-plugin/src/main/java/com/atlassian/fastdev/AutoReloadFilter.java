package com.atlassian.fastdev;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.indexOf;

/**
 * Reloads the plugin if a shift-reload is detected
 */
public class AutoReloadFilter implements Filter
{
    private final Logger LOG = LoggerFactory.getLogger(AutoReloadFilter.class);
    private final ReloadHandler reloadHandler;

    private volatile boolean disabled = false;

    public AutoReloadFilter(ReloadHandler reloadHandler)
    {
        this.reloadHandler = checkNotNull(reloadHandler, "reloadHandler");
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        if (!disabled && isReloadablePath(req))
        {
            if (req.getParameter("reload_die") != null)
            {
                LOG.info("Disabling auto-reload");
                disabled = true;
            }
            else if (isShiftReload(req))
            {
                LOG.info("Reload detected, scanning for changes");
                for (String output : reloadHandler.reloadPlugins())
                {
                    response.setContentType("text/html;charset=utf-8");
                    response.getWriter().write(output);
                    return;
                }
            }
            else
            {
                LOG.info("No reload detected");
            }
        }
        chain.doFilter(request, response);
    }

    private boolean isReloadablePath(HttpServletRequest req)
    {
        String path = req.getRequestURI();
        return path.contains("plugins/servlet") || path.endsWith(".action") || path.endsWith(".jspa");
    }

    private boolean isShiftReload(HttpServletRequest req)
    {
        // "Shift-Reload" is considered a GET request with appropriate cache headers (but without X-Requested-With: XMLHttpRequest)
        // IE returns "no-cache" for the Cache-Control header, but not the Pragma header like other browsers
        return "GET".equals(req.getMethod())
               && indexOf(req.getHeader("X-Requested-With"), "XMLHttpRequest") == -1
               && ("no-cache".equals(req.getHeader("Pragma")) || indexOf(req.getHeader("User-Agent"), "MSIE") != -1)
               && "no-cache".equals(req.getHeader("Cache-Control"));
    }

    public void destroy()
    {
    }
}
