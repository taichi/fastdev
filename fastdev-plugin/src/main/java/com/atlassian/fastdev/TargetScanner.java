package com.atlassian.fastdev;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.fastdev.FastdevProperties.EXTRA_NO_RELOAD_DIRECTORIES_PROP;
import static com.atlassian.fastdev.FastdevProperties.EXTRA_NO_RELOAD_EXTENSIONS_PROP;
import static com.atlassian.fastdev.FastdevProperties.EXTRA_NO_RELOAD_FILES_PROP;
import static com.atlassian.fastdev.FastdevProperties.PLUGIN_ROOT_DIRECTORIES_PROP;
import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.intersection;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.union;
import static org.apache.commons.io.IOCase.SYSTEM;


/**
 * Scans a set of directories, noting any changes that would require a plugin reload
 */
public class TargetScanner
{
    private static final String POM_FILENAME = "pom.xml";
    private static final Logger log = LoggerFactory.getLogger(TargetScanner.class);
    private static final Set<String> NO_RELOAD_DIRS = union(newHashSet(".svn"),
                                                            fetchExtraNoReloadDirectories());

    private final Map<File, Set<ScannedFile>> roots;
    private final Set<String> NO_RELOAD_EXTENSIONS = union(newHashSet("js",
                                                                      "vm",
                                                                      "vmd",
                                                                      "fm",
                                                                      "ftl",
                                                                      "html",
                                                                      "png",
                                                                      "jpeg",
                                                                      "gif",
                                                                      "css",
                                                                      "soy"),
                                                           fetchExtraNoReloadExtensions());
    private final Set<File> NO_RELOAD_FILES = fetchExtraNoReloadFiles();

    /**
     * Loads plugin root directories via the 'plugin.resource.directories' and 'plugin.root.directories' system properties
     */
    public TargetScanner()
    {
        roots = new LinkedHashMap<File, Set<ScannedFile>>();
        addRoots(System.getProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES), true);
        addRoots(System.getProperty(PLUGIN_ROOT_DIRECTORIES_PROP), false);
    }

    private void addRoots(String dirs, boolean isResourceDir)
    {
        if (dirs != null)
        {
            for (String dir : dirs.split(","))
            {
                File file;
                try
                {
                    File pluginDir = new File(dir.trim());
                    if (!pluginDir.exists())
                    {
                        log.warn("Plugin resource directory '" + pluginDir.getPath().toString() + "' does not exist. Ignoring");
                        continue;
                    }
                    else
                    {
                        if (isResourceDir)
                        {
                            /*
                             * Transform plugin/src/main/resources directories to plugin/src/main
                             */
                            File parent = pluginDir.getParentFile();
                            if (parent == null || !parent.exists())
                            {
                                log.warn("Plugin resource directory '" + pluginDir.getPath().toString() + "' does not exist. Ignoring");
                                continue;
                            }
                            file = parent.getCanonicalFile();
                        }
                        else
                        {
                            /*
                             * Transform plugin/ to plugin/src/main
                             */
                            File pom = new File(pluginDir, POM_FILENAME);
                            if (!pom.exists())
                            {
                                log.warn("Plugin source directory '" + pluginDir.getPath() + "' does not contain a pom.xml file. Ignoring");
                                continue;
                            }

                            File srcMainDir = new File(new File(pluginDir, "src"), "main");
                            if (!srcMainDir.exists())
                            {
                                log.warn("Plugin source directory '" + srcMainDir.getPath() + "' does not exist. Ignoring");
                                continue;
                            }
                            file = srcMainDir.getCanonicalFile();
                        }
                    }
                }
                catch (IOException e)
                {
                    log.warn("Unable to find plugin target directory for '" + dir + "'", e);
                    continue;
                }
                if (file.exists() && !roots.containsKey(file))
                {
                    log.info("Found plugin reload target directory '{}'", file);
                    Set<ScannedFile> files = new HashSet<ScannedFile>();
                    scanRoot(file, files);
                    roots.put(file, files);
                }
            }
        }
    }

    private static Set<String> fetchExtraNoReloadExtensions()
    {
        String extensions = System.getProperty(EXTRA_NO_RELOAD_EXTENSIONS_PROP);
        if (extensions != null)
        {
            return ImmutableSet.copyOf(Arrays.asList(extensions.split(",")));
        }
        else
        {
            return ImmutableSet.of();
        }
    }

    private static Set<File> fetchExtraNoReloadFiles()
    {
        String files = System.getProperty(EXTRA_NO_RELOAD_FILES_PROP);

        if (files == null)
        {
            return ImmutableSet.of();
        }

        ImmutableSet.Builder<File> builder = ImmutableSet.builder();
        for (String file : files.split(","))
        {
            try {
                builder.add(new File(file).getCanonicalFile());
            } catch (IOException e) {
                // ignore
            }
        }
        return builder.build();
    }

    private static Set<String> fetchExtraNoReloadDirectories()
    {
        String dirs = System.getProperty(EXTRA_NO_RELOAD_DIRECTORIES_PROP);
        if (dirs != null)
        {
            return ImmutableSet.copyOf(Arrays.asList(dirs.split(",")));
        }
        else
        {
            return ImmutableSet.of();
        }
    }

    /**
     * Get the root plugin target/classes directories found
     */
    public Set<File> getRoots()
    {
        return ImmutableSet.copyOf(roots.keySet());
    }

    /**
     * Scans all known roots, returning roots for which a reload is needed
     */
    public Set<ScanResult> scan()
    {
        ImmutableSet.Builder<ScanResult> rootsToReload = ImmutableSet.builder();

        for (File root : getRoots())
        {
            Set<ScannedFile> files = new HashSet<ScannedFile>();
            scanRoot(root, files);

            Set<ScannedFile> changedFiles = symmetricDifference(files, roots.get(root));

            roots.put(root, files);
            if (changedFilesRequireReload(changedFiles))
            {
                boolean pomChanged = !Collections2.filter(changedFiles, new Predicate<ScannedFile>()
                {
                    @Override
                    public boolean apply(ScannedFile input)
                    {
                        return (null != input && input.getFile().getName().equals("pom.xml"));
                    }
                }).isEmpty();

                File buildRoot = root.getParentFile().getParentFile();
                rootsToReload.add(new ScanResult(buildRoot, root,pomChanged,new File(buildRoot,"pom.xml")));
            }
        }
        return rootsToReload.build();
    }

    // Compute the symmetric difference between two sets. This is in Guava now, but
    // we don't get guava in the products quite yet
    private static <T> Set<T> symmetricDifference(Set<T> setA, Set<T> setB)
    {
        Set<T> either = union(setA, setB);
        Set<T> both = intersection(setA, setB);
        return difference(either, both);
    }

    private boolean changedFilesRequireReload(Set<ScannedFile> changedFiles)
    {
        for (ScannedFile scannedFile : changedFiles)
        {
            if (NO_RELOAD_FILES.contains(scannedFile.getFile()))
            {
                continue;
            }

            String name = scannedFile.getFile().getName();
            String extension = "";
            int ind = name.lastIndexOf('.');
            if (ind > -1)
            {
                extension = name.substring(ind + 1);
            }
            if (!scannedFile.getFile().isDirectory() && !NO_RELOAD_EXTENSIONS.contains(extension))
            {
                return true;
            }
        }
        return false;
    }

    private static void scanRoot(File root, Set<ScannedFile> files)
    {
        // Assumption: the root is a src/main directory of a Maven module
        if (root.isDirectory() && shouldBeScanned(root))
        {
            // Add pom.xml to the set of files
            addScannedFile(new File(root.getParentFile().getParentFile(), POM_FILENAME), files);
            
            scanDir(root, files);
        }
    }
    
    private static void scanDir(File dir, Set<ScannedFile> files)
    {
        if (dir.isDirectory() && shouldBeScanned(dir))
        {
            for (File file : dir.listFiles())
            {
                addScannedFile(file, files);
                scanDir(file, files);
            }
        }
    }
    
    private static boolean shouldBeScanned(File dir)
    {
        String dirName = !SYSTEM.isCaseSensitive() ? dir.getName().toLowerCase() : dir.getName();
        return !NO_RELOAD_DIRS.contains(dirName);
    }
    
    private static void addScannedFile(File file, Set<ScannedFile> files)
    {
        try
        {
            files.add(new ScannedFile(file.getCanonicalFile()));
        }
        catch (IOException e)
        {
            // ignore
        }
    }

    private static class ScannedFile
    {
        private final Long lastModified;
        private final File file;

        public ScannedFile(String filename)
        {
            this(new File(filename));
        }

        public ScannedFile(File file)
        {
            this.file = file;
            this.lastModified = file.lastModified();
        }

        public File getFile()
        {
            return file;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }

            ScannedFile that = (ScannedFile) o;

            if (!file.equals(that.file))
            {
                return false;
            }
            if (!lastModified.equals(that.lastModified))
            {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = lastModified.hashCode();
            result = 31 * result + file.hashCode();
            return result;
        }
    }
}
