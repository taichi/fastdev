package com.atlassian.livereload;

import java.io.IOException;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * This class listens for {@link LiveReloadEvent}s and sends the reload command to the livereload.js via websockets
 * 
 * @since 1.10
 */
public class LiveReloadEventHandler implements InitializingBean,DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(LiveReloadEventHandler.class);
    
    private final LiveReloadServer liveReloadServer;
    private final EventPublisher eventPublisher;

    public LiveReloadEventHandler(LiveReloadServer liveReloadServer, EventPublisher eventPublisher)
    {
        this.liveReloadServer = liveReloadServer;
        this.eventPublisher = eventPublisher;
    }

    public void afterPropertiesSet() throws Exception
    {
        eventPublisher.register(this);
    }
    
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }
    
    @EventListener
    public void onLiveReloadEvent(LiveReloadEvent event)
    {
        try
        {
            //Create a json reload message in LiveReload protocol 7 format and send it 
            Gson gson = new Gson();
            String command = gson.toJson(new ReloadCommand(event.getPath()));

            log.debug("sending command to socket:");
            log.debug(command);
            
            liveReloadServer.sendToAll(command);
        }
        catch (IOException e)
        {
            //if we can't send, just log it as it doesn't obstruct anything
            log.error("Unable to send message via socket server!", e);
            e.printStackTrace();
        }
    }
    
    static class ReloadCommand
    {
        private String command;
        private String path;
        private Boolean liveCSS;

        ReloadCommand(String path)
        {
            this.command = "reload";
            this.path = path;
            this.liveCSS = true;
        }
    }

}
