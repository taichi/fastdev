package com.atlassian.livereload;

/**
 * Simple event to broadcast file change events for live reloading
 * 
 * @since 1.10
 */
public class LiveReloadEvent
{
    private final String path;

    public LiveReloadEvent(String path)
    {
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }
}
