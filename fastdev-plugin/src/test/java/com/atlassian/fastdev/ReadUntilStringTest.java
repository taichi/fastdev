package com.atlassian.fastdev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since version
 */
public class ReadUntilStringTest
{
    @Test
    public void readUntilWorks() throws IOException
    {
        InputStream is = IOUtils.toInputStream("blahblah\nblah\nsome crapp test\nmaven>\nooops");
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        
        StringBuilder sb = new StringBuilder();
        int c;
        char[] endMarker = "maven>".toCharArray();
        char[] endBuffer = new char[endMarker.length];
        int bufLen = 0;
        int markerEndIndex = endMarker.length - 1;
        
        while ((c = br.read()) > -1)
        {
            sb.append((char) c);
            
            if(c == endMarker[bufLen])
            {
                endBuffer[bufLen] = (char) c;

                if(bufLen == markerEndIndex)
                {
                    break;
                }
                bufLen++;
            }
            else
            {
                bufLen = 0;
            }
        }
        
        String readInput = sb.toString();
        assertTrue(readInput.contains("maven>"));
        assertFalse(readInput.contains("ooops"));
    }
}
