package com.atlassian.fastdev.testing;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.fastdev.rest.representations.MavenTaskCollectionRepresentation;
import com.atlassian.sal.api.ApplicationProperties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.uri.UriBuilderImpl;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.RestClient;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;

public class RestTester
{
    private static final String NO_CACHE = "no-cache";

    private final RestClient client;
    private final ApplicationProperties applicationProperties;

    public RestTester()
    {
        this(getStandardApplicationProperties());
    }

    public RestTester(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        ClientConfig config = new ClientConfig();
        config.followRedirects(false);
        client = new RestClient(config);
    }

    public void destroy()
    {
    }

    public int getNumPendingTasks()
    {
        String json = client.resource(buildTasksUri()).accept(MediaType.APPLICATION_JSON).get(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        MavenTaskCollectionRepresentation rep = gson.fromJson(json,MavenTaskCollectionRepresentation.class);
        
        return rep.getTaskCount();
    }

    /**
     * Returns the content of a URI after sending the equivalent of Shift+Reload.
     */
    public String sendShiftReload(String relativeUri)
    {
        return client
            .resource(applicationProperties.getBaseUrl() + relativeUri)
            .header("Pragma", NO_CACHE)
            .header("Cache-Control", NO_CACHE)
            .accept(MediaType.APPLICATION_JSON)
            .get(String.class);
    }

    public void postNoCache(String relativeUri)
    {
        client
            .resource(applicationProperties.getBaseUrl() + relativeUri)
            .header("Pragma", NO_CACHE)
            .header("Cache-Control", NO_CACHE)
            .accept(MediaType.APPLICATION_JSON)
            .post(null);
    }

    public void triggerReload()
    {
        client.resource(buildReloadUri()).accept(MediaType.APPLICATION_JSON).post(null);
    }

    public String getContent(String relativeUri)
    {
        return client
            .resource(applicationProperties.getBaseUrl() + relativeUri)
            .accept(MediaType.APPLICATION_JSON)
            .get(String.class);
    }

    private URI buildReloadUri()
    {
        return newBaseUriBuilder().path("reload").build();
    }

    private URI buildTasksUri()
    {
        return newBaseUriBuilder().path("tasks").build();
    }

    private UriBuilder newBaseUriBuilder()
    {
        return new UriBuilderImpl().replacePath(applicationProperties.getBaseUrl()).path("/rest/fastdev/1.0");
    }
}
