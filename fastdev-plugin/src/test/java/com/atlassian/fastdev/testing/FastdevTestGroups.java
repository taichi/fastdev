package com.atlassian.fastdev.testing;

public class FastdevTestGroups
{
    public static final String BAMBOO = "bamboo";
    public static final String CONFLUENCE = "confluence";
    public static final String FECRU = "fecru";
    public static final String JIRA = "jira";
}
