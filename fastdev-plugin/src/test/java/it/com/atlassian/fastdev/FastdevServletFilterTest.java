package it.com.atlassian.fastdev;

import java.io.IOException;

import com.atlassian.fastdev.testing.FastdevTestUtils;
import com.atlassian.fastdev.testing.RestTester;
import com.atlassian.integrationtesting.runner.TestGroupRunner;

import org.junit.*;
import org.junit.runner.RunWith;

import static com.atlassian.fastdev.testing.FastdevTestUtils.BUNDLE_FILENAME;
import static com.atlassian.fastdev.testing.FastdevTestUtils.SERVLET1_FILENAME;
import static com.atlassian.fastdev.testing.FastdevTestUtils.SERVLET1_URI;
import static com.atlassian.fastdev.testing.FastdevTestUtils.SERVLET2_FILENAME;
import static com.atlassian.fastdev.testing.FastdevTestUtils.SERVLET2_URI;
import static com.atlassian.fastdev.testing.FastdevTestUtils.replaceStringInFile;
import static com.atlassian.fastdev.testing.FastdevTestUtils.touchFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(TestGroupRunner.class)
public class FastdevServletFilterTest
{
    private static final String HELLO = "Hello,";
    private static final String HOWDY = "Howdy,";
    private static final String SAYETH = "sayeth";
    private static final String SAYS = "says";
    private static RestTester restTester;

    @BeforeClass
    public static void setupRestTester()
    {
        restTester = new RestTester();
    }

    @After
    public void waitUntilTasksAreCompleted() throws InterruptedException
    {
        FastdevTestUtils.waitUntilTasksAreCompleted(restTester);
    }

    @AfterClass
    public static void destroyRestTester()
    {
        restTester.destroy();
    }

    @Test
    public void assertThatTargetServletShowsFastdevScreenDuringUpdate()
    {
        touchFile(SERVLET2_FILENAME);
        String content = restTester.sendShiftReload(SERVLET2_URI);

        assertThat(content, containsString("Atlassian FastDev"));
    }

    @Test
    public void assertThatFastdevStartsTwoTasksWhenTwoPluginsAreTouched()
    {
        touchFile(SERVLET1_FILENAME);
        touchFile(SERVLET2_FILENAME);
        restTester.sendShiftReload(SERVLET1_URI);

        assertThat(restTester.getNumPendingTasks(), is(equalTo(2)));
    }

    @Test
    public void assertThatFastdevDoesNotStartTwoTasksWhenOnePluginIsTouched()
    {
        for (int i = 0; i < 2; i++)
        {
            touchFile(SERVLET2_FILENAME);
            restTester.sendShiftReload(SERVLET2_URI);
            assertThat(restTester.getNumPendingTasks(), is(equalTo(1)));
        }
    }

    @Test
    public void assertThatTargetServletIsReloadedAfterPluginSourceIsChanged() throws IOException, InterruptedException
    {
        try
        {
            replaceStringInFile(SERVLET1_FILENAME, SAYS, SAYETH);
            restTester.sendShiftReload(SERVLET1_URI);
            waitUntilTasksAreCompleted();
            String content = restTester.getContent(SERVLET1_URI);

            assertThat(content, containsString(SAYETH));
        }
        finally
        {
            replaceStringInFile(SERVLET1_FILENAME, SAYETH, SAYS);
            restTester.triggerReload();
        }
    }

    //we're ignoring this for now until we can figure out how to get pi to build a bundle
    @Ignore
    @Test
    public void assertThatTargetServletIsReloadedAfterBundleSourceIsChanged() throws IOException, InterruptedException
    {
        try
        {
            replaceStringInFile(BUNDLE_FILENAME, HELLO, HOWDY);
            restTester.sendShiftReload(SERVLET1_URI);
            waitUntilTasksAreCompleted();
            // The next three lines (reloading the plugin consuming the bundle) are necessary until FASTDEV-43 is fixed
            // (dependency resolution for multi-plugin projects).
            touchFile(SERVLET1_FILENAME);
            restTester.sendShiftReload(SERVLET1_URI);
            waitUntilTasksAreCompleted();
            String content = restTester.getContent(SERVLET1_URI);

            assertThat(content, containsString(HOWDY));
        }
        finally
        {
            replaceStringInFile(BUNDLE_FILENAME, HOWDY, HELLO);
            restTester.triggerReload();
        }
    }

    @Test
    public void assertThatPostRequestDoesNotTriggerFastDev()
    {
        try
        {
            touchFile(SERVLET2_FILENAME);
            restTester.postNoCache(SERVLET2_URI);
            assertThat(restTester.getNumPendingTasks(), is(equalTo(0)));
        }
        finally
        {
            restTester.triggerReload();
        }
    }
}
